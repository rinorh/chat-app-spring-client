package integrationTests;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import root.DTO.RoomNameTransport;
import root.client.ChatClient;
import root.client.HttpHandler;
import root.client.LocalStorage;

import static org.junit.Assert.assertEquals;

public class RoomIT {

    private HttpHandler httpHandler = new HttpHandler();

    @Before
    public void setup() {
        LocalStorage.setToken("4");
    }

    @Test
    public void getRooms_withNoArgs_return200OK() {
        ResponseEntity<String> response = httpHandler.makeGetRequest(ChatClient.URI + "/rooms/", LocalStorage.getToken());
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void getUser_byId_return200OK() {
        ResponseEntity<String> response = httpHandler.makeGetRequest(ChatClient.URI + "/rooms/1", LocalStorage.getToken());
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void createRoom_byRoomTransport_return200OK() {
        RoomNameTransport roomTransport = new RoomNameTransport("testRoom");
        ResponseEntity<String> response = httpHandler.makePostRequest(ChatClient.URI + "/rooms/", roomTransport, LocalStorage.getToken());
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void updateRoom_byRoomTransport_return200OK() {
        RoomNameTransport roomTransport = new RoomNameTransport("updatedRoom");
        ResponseEntity<String> response = httpHandler.makePutRequest(ChatClient.URI + "/rooms/1", roomTransport, LocalStorage.getToken());
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

}
