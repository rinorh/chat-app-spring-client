package integrationTests;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import root.DTO.UserCredentialsTransport;
import root.DTO.UserRoomUpdationTransport;
import root.client.ChatClient;
import root.client.HttpHandler;
import root.client.LocalStorage;

import static org.junit.Assert.assertEquals;

public class UserIT {

    private HttpHandler httpHandler = new HttpHandler();

    @Before
    public void setup() {
        LocalStorage.setToken("1");
    }

    @Test
    public void getUsers_withNoArgs_return200OK() {
        ResponseEntity<String> response = httpHandler.makeGetRequest(ChatClient.URI + "/users/", LocalStorage.getToken());
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void getUser_byId_return200OK() {
        ResponseEntity<String> response = httpHandler.makeGetRequest(ChatClient.URI + "/users/1", LocalStorage.getToken());
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void updateUserCredentials_byCredentials_return200OK() {
        ResponseEntity<String> response = httpHandler.makePutRequest(ChatClient.URI + "/users/16/credentials", new UserCredentialsTransport("lol", "lol123"), "16");
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void updateUserRoom_byUserRoomUpdationTransport_return200OK() {
        ResponseEntity<String> response = httpHandler.makePutRequest(ChatClient.URI + "/users/1", new UserRoomUpdationTransport("1"), LocalStorage.getToken());
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

}
