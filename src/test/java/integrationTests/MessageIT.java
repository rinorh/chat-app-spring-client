package integrationTests;

import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import root.DTO.MessageContentTransport;
import root.client.ChatClient;
import root.client.HttpHandler;
import root.client.LocalStorage;

import static org.junit.Assert.*;

@SpringBootTest
public class MessageIT {

    private HttpHandler httpHandler = new HttpHandler();

    @Before
    public void setup() {
        LocalStorage.setToken("1");
    }

    @Test
    public void getMessages_withNoArg_return200OK() {
        ResponseEntity<String> response = httpHandler.makeGetRequest(ChatClient.URI + "/messages/", LocalStorage.getToken());
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void getMessage_byId_return200OK() {
        ResponseEntity<String> response = httpHandler.makeGetRequest(ChatClient.URI + "/messages/50", LocalStorage.getToken());
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void getUnread_byLastTimeRead_return200OK() {
        ResponseEntity<String> response = httpHandler.makeGetRequest(ChatClient.URI + "/messages?lastTimeRead=0", LocalStorage.getToken());
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void sendMessage_byMessageSentObj_return200OK() {
        MessageContentTransport message = new MessageContentTransport("hejTest");
        httpHandler.makePostRequest(ChatClient.URI + "/messages/", message, LocalStorage.getToken());
    }

}

/*
    deleteOldMessagesInApp(); {eachMonth}
        for all rooms:
            findOldMessagesAndDeleteThem()[1]


     [1]: findOldMessagesAndDeleteThem():
            forAllMessagesInRoom:
                checkIfMessageIsOldAndDeleteIt():

     [2]: checkIfMessageIsOldAndDeleteIt():
            checkIfMessageHasBeenReadByAllUsers (if true then delete it)

     [3]: checkIfMessageHasBeenReadByAllUsers():
            return true if each user in room has Seen It:

     [4]: checkIfEachUserInRoomHasSeenIt():
            for all users ever in this room:
                checkIfUserHasSeenMessage():

     [5]: checkIfUserHasSeenMessage():
            compareMessageTimestamp and user_room_last_time_in_room:

            CREATE TABLE room_visit (
               id INT NOT NULL AUTO_INCREMENT,
               room_id INT NULL,
               user_id INT NULL,
               last_time_in_room TIMESTAMP,
               PRIMARY KEY(id),
               INDEX(room_id),
               FOREIGN KEY(room_id)
               REFERENCES room(id),
               INDEX(user_id),
               FOREIGN KEY (user_id)
               REFERENCES user(id)
            ) AUTO_INCREMENT = 1 ENGINE = INNODB;
*/
