package integrationTests;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import root.DTO.UserCredentialsTransport;
import root.client.ChatClient;
import root.client.HttpHandler;

import static org.junit.Assert.*;

public class AuthenticationIT {

    private HttpHandler httpHandler = new HttpHandler();

    @Test
    public void register_withUsernameAndPassword_return200OK() {
        UserCredentialsTransport user = new UserCredentialsTransport("testUser", "user123");
        ResponseEntity<String> response = httpHandler.makePostRequest(ChatClient.URI + "/users/", user);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void login_withUsernameAndPassword_return200OK() {
        UserCredentialsTransport user = new UserCredentialsTransport("rinor", "rinori123");
        ResponseEntity<String> response = httpHandler.makePostRequest(ChatClient.URI + "/auth/", user);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test(expected = HttpClientErrorException.Unauthorized.class)
    public void login_withUsernameAndPassword_return401UNAUTHORIZED() throws HttpClientErrorException.Unauthorized {
        UserCredentialsTransport user = new UserCredentialsTransport("sdfjnsjf", "sdjnfsjdfn");
        HttpEntity<Object> request = new HttpEntity<>(user);
        new RestTemplate().exchange(ChatClient.URI + "/auth/", HttpMethod.POST, request, String.class);
    }

}
