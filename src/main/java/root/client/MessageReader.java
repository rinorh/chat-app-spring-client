package root.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import root.DTO.MessageListTransport;
import root.DTO.MessageTransport;

import java.io.IOException;
import java.util.List;

@Component
public class MessageReader {

    private HttpHandler httpHandler;
    private ObjectMapper objectMapper = new ObjectMapper();

    public MessageReader(HttpHandler httpHandler) {
        this.httpHandler = httpHandler;
    }

    @Scheduled(fixedRate = 1000L)
    public void read() {
        if(LocalStorage.getRoomID() != null && LocalStorage.getToken() != null) {
            if(getAllUnreadMessages(LocalStorage.getToken())) {
                LocalStorage.setLastTimeReadMessages(System.currentTimeMillis());
            }
        }
    }

    private boolean getAllUnreadMessages(String token) {
        ResponseEntity<String> response = httpHandler.makeGetRequest(ChatClient.URI + "/messages?lastTimeRead=" + LocalStorage.getLastTimeReadMessages(), token);
        try {
            List<MessageTransport> messages = objectMapper.readValue(response.getBody(), MessageListTransport.class).getMessages();
            messages.forEach(message -> System.out.println(message.getUsername() + ": " + message.getContent()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return response.getStatusCode() == HttpStatus.OK;
    }

}
