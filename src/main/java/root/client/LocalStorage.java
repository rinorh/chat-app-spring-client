package root.client;

public class LocalStorage {

    private static String token;
    private static String roomID;
    private static long lastTimeReadMessages;

    public static void setToken(String userToken) {
        token = userToken;
    }

    public static void setRoomID(String chatRoomID) {
        roomID = chatRoomID;
    }

    public static void setLastTimeReadMessages(long lastTimeReadMessagesTime) {
        lastTimeReadMessages = lastTimeReadMessagesTime;
    }

    public static void removeToken() {
        token = null;
    }

    public static void removeRoomID() {
        roomID = null;
    }

    public static void removeLastTimeReadMessages() {
        lastTimeReadMessages = 0;
    }

    public static String getToken() {
        return token;
    }

    public static String getRoomID() {
        return roomID;
    }

    public static long getLastTimeReadMessages() {
        return lastTimeReadMessages;
    }

}
