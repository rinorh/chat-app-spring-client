package root.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import root.DTO.*;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

@Component
public class ChatClient implements Runnable {
    private static final String API_VERSION = "v1";
    public static final String URI = "http://localhost:8080/" + API_VERSION;

    private Scanner scanner = new Scanner(System.in);
    private ObjectMapper objectMapper = new ObjectMapper();
    private HttpHandler httpHandler = new HttpHandler();

    public ChatClient() { }

    @Override
    public void run() {
        startApp();
    }

    public void startApp() {
        System.out.println("Type LOGIN to login, REGISTER to register or EXIT to exit the app: ");
        String action = scanner.nextLine();
        if(action.equals("EXIT")) System.exit(0);
        if(action.equals("LOGIN")) {
            login();
        } else if(action.equals("REGISTER")) {
            if(register()) {
                login();
            } else {
                System.out.println("Registering failed!");
            }
        } else {
            System.out.println("WRONG COMMAND!");
            startApp();
        }
    }

    private void login() {
        System.out.print("Enter username: ");
        String username = scanner.nextLine();
        System.out.print("Enter password: ");
        String password = scanner.nextLine();
        if(login(username, password)) {
            chooseRoom();
        } else {
            login();
        }
    }

    private boolean login(String username, String password) {
        UserCredentialsTransport userCredentials = new UserCredentialsTransport(username, password);
        ResponseEntity<String> response = httpHandler.makePostRequest(URI + "/auth/", userCredentials);
        if(response == null) return false;
        try {
            LocalStorage.setToken(objectMapper.readValue(response.getBody(), UserTokenTransport.class).getToken());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return true;
    }

    private void chooseRoom() {
        System.out.println("Press the corresponding number to enter a room, create to create a room or logout to logout: ");
        getRooms();
        String action = scanner.nextLine();
        if(action.equals("logout")) {
            logout();
        } else if(action.equals("create")) {
            createRoom();
        } else  {
            if(enterRoom(action)) {
                startChatting();
            } else {
                System.out.println("Select a valid room number!");
                chooseRoom();
            }
        }
    }

    private void createRoom() {
        System.out.println("Enter a room name that your want to create: ");
        String roomName = scanner.nextLine();
        if(createRoom(roomName)) {
            System.out.println("Room " + roomName + " created!");
            chooseRoom();
        } else {
            System.out.println("Room could not be created!");
            createRoom();
        }
    }

    public boolean createRoom(String roomName) {
        ResponseEntity<String> response = httpHandler.makePostRequest(URI + "/rooms/", new RoomNameTransport(roomName), LocalStorage.getToken());
        RoomIdTransport roomTransport = null;
        try {
            roomTransport = objectMapper.readValue(response.getBody(), RoomIdTransport.class);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return response.getStatusCode() == HttpStatus.OK;
    }

    private void startChatting() {
        String message = null;
        do {
            if(hasTypedKeyWord(message)) break;
            message = scanner.nextLine();
            if(message.equals("LEAVE")) leaveRoom();
            else if(message.equals("LOGOUT")) logout();
            else sendMessage(message);
        } while(!hasTypedKeyWord(message));
    }

    private boolean hasTypedKeyWord(String message) {
        return message != null && (message.equals("LEAVE") || message.equals("LOGOUT"));
    }

    private void getRooms() {
        ResponseEntity<String> response = httpHandler.makeGetRequest(URI + "/rooms/", LocalStorage.getToken());
        try {
            List<RoomTransport> rooms = objectMapper.readValue(response.getBody(), RoomListTransport.class).getRooms();
            rooms.forEach(room -> System.out.println(room.getId() + ": " + room.getName()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private boolean enterRoom(String roomID) {
        ResponseEntity<String> response = httpHandler.makePutRequest(URI + "/users/" + LocalStorage.getToken(), new UserRoomUpdationTransport(roomID), LocalStorage.getToken());
        if(response == null) return false;
        if(response.getStatusCode() == HttpStatus.OK) {
            try {
                LocalStorage.setRoomID(objectMapper.readValue(response.getBody(), UserRoomUpdationTransport.class).getRoomId());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            return true;
        }
        return false;
    }

    private void leaveRoom() {
        httpHandler.makePutRequest(URI + "/users/" + LocalStorage.getToken(), new UserRoomUpdationTransport(null), LocalStorage.getToken());
        LocalStorage.removeRoomID();
        LocalStorage.removeLastTimeReadMessages();
        chooseRoom();
    }

    private boolean register() {
        System.out.print("Choose a username: ");
        String username = scanner.nextLine();
        System.out.print("Choose a password: ");
        String password = scanner.nextLine();
        return register(username, password);
    }

    private boolean register(String username, String password) {
        UserTransport user = new UserTransport(username, password);
        ResponseEntity<String> response = httpHandler.makePostRequest(URI + "/users/", user);
        return response.getStatusCode() == HttpStatus.OK;
    }

    private void sendMessage(String content) {
        MessageContentTransport message = new MessageContentTransport(content);
        httpHandler.makePostRequest(URI + "/messages/", message, LocalStorage.getToken());
    }

    private void logout() {
        LocalStorage.removeRoomID();
        LocalStorage.removeLastTimeReadMessages();
        LocalStorage.removeToken();
        startApp();
    }

}
