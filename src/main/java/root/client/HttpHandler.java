package root.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import root.exceptions.ClientRequestException;

@Component
public class HttpHandler {

    private RestTemplate restTemplate = new RestTemplate();

    public ResponseEntity<String> makeGetRequest(String url, String token) {
        ResponseEntity<String> responseEntity = null;
        try {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setBearerAuth(token);
            HttpEntity request = new HttpEntity(httpHeaders);
            responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return responseEntity;
    }


    public ResponseEntity<String> makePostRequest(String url, Object obj) {
        ResponseEntity<String> responseEntity = null;
        try {
            HttpEntity<Object> request = new HttpEntity<>(obj);
            responseEntity = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
        } catch (HttpClientErrorException.Unauthorized ex) {
            System.out.println("Wrong username or password!");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return responseEntity;
    }

    public ResponseEntity<String> makePostRequest(String url, String token) {
        ResponseEntity<String> responseEntity = null;
        try {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setBearerAuth(token);
            HttpEntity request = new HttpEntity(httpHeaders);
            responseEntity = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
            if(responseEntity == null) throw new ClientRequestException("Response Entity: null");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return responseEntity;
    }

    public ResponseEntity<String> makePostRequest(String url, Object obj, String token) {
        ResponseEntity<String> responseEntity = null;
        try {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setBearerAuth(token);
            HttpEntity<Object> request = new HttpEntity<>(obj, httpHeaders);
            responseEntity = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
            if(responseEntity == null) throw new ClientRequestException("Response Entity: null");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return responseEntity;
    }

    public ResponseEntity<String> makePutRequest(String url, Object obj, String token) {
        ResponseEntity<String> responseEntity = null;
        try {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setBearerAuth(token);
            HttpEntity<Object> request = new HttpEntity<>(obj, httpHeaders);
            responseEntity = restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
            if(responseEntity == null) throw new ClientRequestException("Response Entity: null");
        } catch (HttpClientErrorException.NotFound ex) {
            System.out.println("No room found with the given ID!");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return responseEntity;
    }

    public String makeDeleteRequest(String url) {
        return null;
    }

}
