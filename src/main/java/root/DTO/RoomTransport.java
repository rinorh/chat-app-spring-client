package root.DTO;

public class RoomTransport {

    private int id;
    private String name;

    public RoomTransport() { }

    public RoomTransport(String name) {
        this.name = name;
    }

    public RoomTransport(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "RoomTransport{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

}
