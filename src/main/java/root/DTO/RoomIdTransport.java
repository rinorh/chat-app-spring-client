package root.DTO;

public class RoomIdTransport {

    private int roomId;

    public RoomIdTransport() { }

    public RoomIdTransport(int roomId) {
        this.roomId = roomId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    @Override
    public String toString() {
        return "RoomIdTransport{" +
                "roomId=" + roomId +
                '}';
    }

}
