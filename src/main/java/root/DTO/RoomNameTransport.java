package root.DTO;

public class RoomNameTransport {

    private String roomName;

    public RoomNameTransport() { }

    public RoomNameTransport(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    @Override
    public String toString() {
        return "RoomNameTransport{" +
                "roomName='" + roomName + '\'' +
                '}';
    }

}
