package root.DTO;

public class InfoTransport {

    private String message;

    public InfoTransport() { }

    public InfoTransport(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "RegistrationInfoTransport{" +
                "message='" + message + '\'' +
                '}';
    }

}
