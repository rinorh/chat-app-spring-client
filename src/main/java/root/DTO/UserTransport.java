package root.DTO;

public class UserTransport {

    private String username;
    private String password;

    public UserTransport() { }

    public UserTransport(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserTransport{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

}
