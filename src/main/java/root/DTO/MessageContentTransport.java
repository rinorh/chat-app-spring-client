package root.DTO;

public class MessageContentTransport {

    private String content;

    public MessageContentTransport() { }

    public MessageContentTransport(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "MessageContentTransport{" +
                "content='" + content + '\'' +
                '}';
    }

}
