package root.DTO;

import java.util.List;

public class RoomListTransport {

    private List<RoomTransport> rooms;

    public RoomListTransport() { }

    public RoomListTransport(List<RoomTransport> rooms) {
        this.rooms = rooms;
    }

    public List<RoomTransport> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomTransport> rooms) {
        this.rooms = rooms;
    }

    @Override
    public String toString() {
        return "RoomListTransport{" +
                "rooms=" + rooms +
                '}';
    }

}
