package root.DTO;

public class UserRoomUpdationTransport {

    private String roomId;

    private UserRoomUpdationTransport() { }

    public UserRoomUpdationTransport(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

}
