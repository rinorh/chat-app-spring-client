package root.DTO;

import java.util.List;

public class UserListTransport {

    private List<UserTransport> users;

    public UserListTransport() { }

    public UserListTransport(List<UserTransport> users) {
        this.users = users;
    }

    public List<UserTransport> getUsers() {
        return users;
    }

    public void setUsers(List<UserTransport> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "UserListTransport{" +
                "users=" + users +
                '}';
    }

}
