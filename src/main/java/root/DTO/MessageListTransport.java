package root.DTO;

import java.util.List;

public class MessageListTransport {

    private List<MessageTransport> messages;

    public MessageListTransport() { }

    public MessageListTransport(List<MessageTransport> messages) {
        this.messages = messages;
    }

    public List<MessageTransport> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageTransport> messages) {
        this.messages = messages;
    }

    @Override
    public String toString() {
        return "MessageListTransport{" +
                "messages=" + messages +
                '}';
    }

}
