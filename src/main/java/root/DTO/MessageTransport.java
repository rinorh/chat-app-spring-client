package root.DTO;

import java.util.List;

public class MessageTransport {

    private String username;
    private String content;
    private List<UserTransport> usersSeen;

    public MessageTransport() {}

    public MessageTransport(String username, String content) {
        this.username = username;
        this.content = content;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<UserTransport> getUsersSeen() {
        return usersSeen;
    }

    public void setUsersSeen(List<UserTransport> usersSeen) {
        this.usersSeen = usersSeen;
    }

    @Override
    public String toString() {
        return "MessageTransport{" +
                "username='" + username + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

}
