package root;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import root.client.ChatClient;
import root.client.MessageReader;

@SpringBootApplication
@EnableScheduling
public class Application {

    private MessageReader messageReader;

    public Application(MessageReader messageReader) {
        this.messageReader = messageReader;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        Thread chatClient = new Thread(new ChatClient());
        chatClient.start();
    }

}

